import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './core';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch:'full'},
  { path: 'login',   loadChildren: () => import('./components/login-page/login-page.module').then(mod => mod.LoginPageModule)},
  { path: 'home',  loadChildren: () => import('./components/home/home.module').then(mod => mod.HomeModule),  canActivate: [AuthGuardService]},
  { path: 'registration', loadChildren: () => import('./components/registration/registration.module').then(mod => mod.RegistrationModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
  