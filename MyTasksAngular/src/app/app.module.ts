import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StorageServiceModule } from 'ngx-webstorage-service';
import { MatNativeDateModule } from '@angular/material/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDialogModule } from '@angular/material/dialog';
import { CoreModule } from './core/core.module';
import { LoginPageModule } from './components/login-page/login-page.module';
import { AuthenticationModule } from './components/authentication/authentication.module';
import { TasksModule } from './components/tasks/tasks.module';
import { HomeModule } from './components/home/home.module';
import { MapModule } from './components/map/map.module';
import {  MatIconModule } from '@angular/material/icon';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { SERVER_URL } from '../environments/environment';

const config: SocketIoConfig = { url: SERVER_URL, options: {}};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CoreModule,
    LoginPageModule,
    AuthenticationModule,
    TasksModule,
    HomeModule,
    MapModule,

    MatDialogModule,
    MatSidenavModule,
    BrowserAnimationsModule,
    MatNativeDateModule,
    StorageServiceModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatIconModule,
    SocketIoModule.forRoot(config)
  ],
  exports: [
    BrowserAnimationsModule,
    MatNativeDateModule,
    BrowserModule,
    MatSidenavModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
