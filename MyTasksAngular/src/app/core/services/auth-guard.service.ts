import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanLoad, UrlSegment, CanActivateChild } from '@angular/router';
import { Injectable } from '@angular/core';
import { AccountService } from './account.service';
import { Route } from '@angular/compiler/src/core';

@Injectable({
  providedIn: 'root'
})

export class AuthGuardService implements CanActivate, CanLoad, CanActivateChild {

  constructor(private accountService: AccountService,
    private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    const isUserAuthenticated = this.accountService.isAuthenticated();

    if (!isUserAuthenticated) {

      this.router.navigate(['login']);
      return false;
    }

    return true;
  }

  canLoad(route: Route, segments: UrlSegment[]){

    return true;
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    return true;
  }

}
