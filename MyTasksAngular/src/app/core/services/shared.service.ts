import { Injectable, EventEmitter } from '@angular/core';
import { MenuAction } from '../enums/local/menu-action.enum';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  public onMenuItemSelected = new EventEmitter<MenuAction>();
  public onMenuSearchPerfomed = new EventEmitter<string>();

  constructor() { }
}
