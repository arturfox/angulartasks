import { Injectable, Inject } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { LocalStorageService } from './local-storage.service';
import { TasksListModel } from '../models/tasks/local/tasks-list.model';

@Injectable({
  providedIn: 'root'
})
export class TasksLocalService extends LocalStorageService {

  private TASKS_LIST_KEY = 'taskslist';

  constructor(@Inject(LOCAL_STORAGE) storage: StorageService) {

    super(storage);
  }

  public UpdateUsersTaks(model: TasksListModel) {

    super.write(this.TASKS_LIST_KEY, model);
  }

  public GetUsersTasks(): TasksListModel {

    let tasks = this.get(this.TASKS_LIST_KEY);
    return (tasks as TasksListModel);
  }

  public ClearUsersTasks(){

    this.remove(this.TASKS_LIST_KEY);
  }

}
