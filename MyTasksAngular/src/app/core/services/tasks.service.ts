import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpHelper } from '../helpers/httphelper';
import { ResultModel } from '../models/api/result-model';
import { GetTasksModel } from '../models/tasks/api/get-tasks-model';
import { CreateTaskModel } from '../models/tasks/api/creteate-task.model';
import { GetTasksModelItem } from '../models/tasks/api/get-tasks-model.item';
import { UpdateTaskStatusModel } from '../models/tasks/api/update-task-status.model';
import { DeleteTaskModel } from '../models/tasks/api/delete-task.model';
import { EditTaskModel } from '../models/tasks/api/edit-task.model';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class TasksService extends BaseService {

    constructor(private httphelper: HttpHelper) {

        super();
    }

    public getUserTasks(): Observable<ResultModel<GetTasksModel>> {

        const route = "tasks/getTasks/";      
        return this.httphelper.Get<GetTasksModel>(`${route}`);
    }

    public createTask(model: CreateTaskModel): Observable<ResultModel<GetTasksModelItem>> {

        const route = "tasks/addTask";
        return this.httphelper.Post<GetTasksModelItem>(`${route}`, model);
    }

    public updateTaskStatus(updateModel: UpdateTaskStatusModel): Observable<ResultModel<GetTasksModelItem>> {

        const route = "tasks/updateTaskStatus/";
        return this.httphelper.Post<GetTasksModelItem>(`${route}`, updateModel);
    }

    public deleteTaskById(deleteModel: DeleteTaskModel): Observable<ResultModel> {

        const route = "tasks/deleteTask";
        return this.httphelper.Post<void>(`${route}`, deleteModel);
    }

    public editTask(editmodel: EditTaskModel): Observable<ResultModel<GetTasksModelItem>> {

        const route = "tasks/editTask";
        return this.httphelper.Post<GetTasksModelItem>(`${route}`, editmodel);
    }
}