import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { AuthService, SocialUser } from 'angularx-social-login';
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { ResponseLoginModel } from '../models/authentication/api/response-login.model';
import { RequestLoginBaseModel } from '../models/authentication/api/request-login-base.model';
import { RegisterModel } from '../models/authentication/api/register.model';
import { HttpHelper } from '../helpers/httphelper';
import { ResultModel } from '../models/api/result-model';
import { AuthenticationType } from '../enums/authentication-type.enum';
import { LocalUserService } from './local-user.service';
import { Observable, from, of } from 'rxjs';
import { RequestRefreshTokenModel } from '../models/authentication/api/request-refresh-token.model';
import { catchError, switchMap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AccountService extends BaseService {

    constructor(private httphelper: HttpHelper,
        private authService: AuthService,
        private localUserService: LocalUserService) {

        super();
    }

    public loginGooglePlus(): Observable<ResultModel<ResponseLoginModel>> {

        return this.authorizateByType(AuthenticationType.GooglePlus);
    }

    public loginFacebook(): Observable<ResultModel<ResponseLoginModel>> {

        return this.authorizateByType(AuthenticationType.FaceBook);
    }


    public login(model: RequestLoginBaseModel): Observable<ResultModel<ResponseLoginModel>> {

        const route = "account/login/";
        return this.httphelper.Post<ResponseLoginModel>(`${route}`, model);
    }

    public refreshToken(model: RequestRefreshTokenModel): Observable<ResultModel<ResponseLoginModel>> {

        const route = "account/token/refresh";
        return this.httphelper.Post(`${route}`, model, false);
    }

    public signIn(model: RegisterModel): Observable<ResultModel> {

        const route = "account/register/";
        return this.httphelper.Post(`${route}`, model);
    }


    public async logOut(type: AuthenticationType): Promise<ResultModel> {

        const route = "account/logout";

        try {

            const data = await this.httphelper.Get<void>(`${route}`).toPromise();
            const result: ResultModel<void> = data;

            const logoutData = (type == AuthenticationType.FaceBook || type == AuthenticationType.GooglePlus)
                ? (await this.authService.signOut()) : null;

            return result;
        }
        catch (e) {

            let errorModel = this.getErrorModel<void>(`${route}`);
            return errorModel;
        }
    }

    private authorizateByType(type: AuthenticationType): Observable<ResultModel<ResponseLoginModel>> {

        let authorizationResult = new ResultModel<ResponseLoginModel>();
        let authResponce = (type == AuthenticationType.FaceBook) ? (from(this.authService.signIn(FacebookLoginProvider.PROVIDER_ID)))
            : (from(this.authService.signIn(GoogleLoginProvider.PROVIDER_ID)));

        return (authResponce.pipe(catchError((error) => { return null; }),
            switchMap((authResult) => { return of(authResult) }),
            switchMap((user: SocialUser) => {
                if (user == null) {
                    authorizationResult.isSuceed = false;
                    authorizationResult.message = 'cannot get user information';

                    return of(authorizationResult);
                }

                let requestData = new RequestLoginBaseModel();
                requestData.username = user.email;
                requestData.authorizationType = type;
                requestData.accessToken = user.authToken;

                return this.login(requestData);
            })));
    }

    public isAuthenticated(): boolean {

        const userInfo = this.localUserService.GetCurrentUser();

        console.log(userInfo);

        if (userInfo == null || userInfo.token == null) {

            return false;
        }

        return true;
    }
} 