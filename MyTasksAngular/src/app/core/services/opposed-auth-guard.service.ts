import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanLoad, UrlSegment, CanActivateChild } from '@angular/router';
import { AccountService } from './account.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OpposedAuthGuardService implements CanActivate {

  constructor(private accountService: AccountService,
    private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    const isUserAuthenticated = this.accountService.isAuthenticated();

    if (isUserAuthenticated) {

      this.router.navigate(['home']);
      return false;
    }

    return true;
  }
}
