import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  private TOAST_DURATION_MILLISECONDS = 3000;

  constructor(private _snackBar: MatSnackBar) {

  }

  showMessage(message: string) {

    this._snackBar.open(message, null, {
      duration: this.TOAST_DURATION_MILLISECONDS,
    });

  }

}
