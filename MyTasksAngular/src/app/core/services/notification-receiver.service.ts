import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { LocalUserService } from './local-user.service';

@Injectable({
  providedIn: 'root'
})
export class NotificationReceiverService {

  constructor(private socket: Socket,
    private localUserService: LocalUserService) {

  }

  receiveNotifications() {

    const userId = this.localUserService.GetCurrentUser().userId;

    return this.socket.fromEvent(`refresh/${userId}`);
  }

}
