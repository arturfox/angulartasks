import { Injectable, Inject } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) {

  }

  public clearStorage(){

    this.storage.clear();
  }

  protected write(key: string, data: any):void {

    this.storage.set(key,data);
  }

  protected get(key: string):any {

    let data = this.storage.get(key);
    return data;
  }

  protected remove(key: string):void {

    this.storage.remove(key);
  }

}
