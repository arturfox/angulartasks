import { Injectable } from '@angular/core';
import { IBaseService } from './ibase.service';
import { ResultModel } from '../models/api/result-model';

@Injectable({
    providedIn: 'root'
  })
export class BaseService implements IBaseService {

    constructor(){

    }

    getErrorModel<T>(route: string): ResultModel<T>{

        var errorModel=new ResultModel<T>();
        errorModel.isSuceed=false;
        errorModel.statusCode=500;
        errorModel.message=`en unknow error occurred while refering to ${route}`;

        return errorModel; 
    }
}
