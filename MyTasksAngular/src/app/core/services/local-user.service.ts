import { Injectable, Inject } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { LocalStorageService } from '../../core/services/local-storage.service';
import { LocalUser } from '../models/local-user';

@Injectable({
  providedIn: 'root'
})
export class LocalUserService extends LocalStorageService {

  private LOCAL_USER_KEY = 'user';

  constructor(@Inject(LOCAL_STORAGE) storage: StorageService) {

    super(storage);
  }

  public saveCurrentUserInfo(user: LocalUser) {

    super.write(this.LOCAL_USER_KEY, user);
  }

  public GetCurrentUser(): LocalUser {

    let user = this.get(this.LOCAL_USER_KEY);
    return user;
  }

  public ClearUserInfo(){

    this.remove(this.LOCAL_USER_KEY);
  }

}
