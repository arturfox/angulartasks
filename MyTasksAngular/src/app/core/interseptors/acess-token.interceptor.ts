import { Injectable, } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { LocalUserService } from '../services/local-user.service';

@Injectable()
export class AcessTokenInterceptor implements HttpInterceptor {
    constructor(private localUserService: LocalUserService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler) {

        if (
            req.url.includes("account/token/refresh") ||
            req.url.includes("account/login") ||
            req.url.includes("account/register") ||
            req.url.includes("assets/")) {

            return next.handle(req);
        }

        const user = this.localUserService.GetCurrentUser();

        if (user) {

            const authReq = req.clone({
                headers: req.headers.set('Authorization', `Bearer ${user.token}`)
            });

            return next.handle(authReq);
        }

        return next.handle(req);
    }
}