import { Injectable, } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { ToastService } from '../services';

@Injectable()
export class HttpErrorInterseptor implements HttpInterceptor {
    constructor(private toastService: ToastService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler) {

        if (req.url.includes("assets/") ||
            req.url.includes("account/token/refresh")) {
            return next.handle(req);
        }

        return next.handle(req).pipe(
            catchError((err: HttpErrorResponse) => {

                this.toastService.showMessage(`An error occurred: ${err.error.message}`);
                return next.handle(req);
            }));
    }
}