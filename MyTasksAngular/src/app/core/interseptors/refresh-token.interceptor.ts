import { Injectable } from "@angular/core";
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from "@angular/common/http";
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { switchMap, filter, take, catchError } from 'rxjs/operators';
import { RequestRefreshTokenModel } from '../models/authentication/api/request-refresh-token.model';
import { LocalUserService } from '../services/local-user.service';
import { AccountService } from '../services/account.service';
import { ResponseLoginModel } from '../models/authentication/api/response-login.model';
import { LocalUser } from '../models/local-user';
import { Router } from '@angular/router';
import { LocalStorageService } from '../services';

@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {
  private refreshTokenInProgress = false;
  // Refresh Token Subject tracks the current token, or is null if no token is currently
  // available (e.g. refresh pending).
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(
    null
  );
  constructor(private auth: AccountService,
    private localUserService: LocalUserService,
    private router: Router,
    private localStorageService: LocalStorageService) { }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    console.log("RefreshTokenInterceptor fired");

    return next.handle(request).pipe(
      catchError(err => {
        // We don't want to refresh token for some requests like login or refresh token itself
        // So we verify url and we throw an error if it's the case

        if (request.url.includes("account/token/refresh") ||
          request.url.includes("account/login") ||
          request.url.includes("account/register") ||
          request.url.includes("assets/")) {
          // We do another check to see if refresh token failed
          // In this case we want to logout user and to redirect it to login page

          if (request.url.includes("account/token/refresh")) {

            this.logOut();
          }

          return throwError(err);
        }

        // If error status is different than 401 we want to skip refresh token
        // So we check that and throw the error if it's the case
        if (err.status !== 401) {

          return throwError(err);
        }

        if (this.refreshTokenInProgress) {
          // If refreshTokenInProgress is true, we will wait until refreshTokenSubject has a non-null value
          // – which means the new token is ready and we can retry the request again

          return this.refreshTokenSubject.pipe(
            filter(result => result !== null),
            take(1),
            switchMap(() => next.handle(this.addAuthenticationToken(request))));
        } else {

          this.refreshTokenInProgress = true;

          // Set the refreshTokenSubject to null so that subsequent API calls will wait until the new token has been retrieved
          this.refreshTokenSubject.next(null);

          const requestModel = new RequestRefreshTokenModel();
          requestModel.refreshToken = this.localUserService.GetCurrentUser().refreshToken;

          // Call auth.refreshAccessToken(this is an Observable that will be returned)
          return this.auth
            .refreshToken(requestModel).pipe(
              switchMap((tokenData: any) => {
                //When the call to refreshToken completes we reset the refreshTokenInProgress to false
                // for the next time the token needs to be refreshed
                this.saveUserInfo(tokenData.data);

                this.refreshTokenInProgress = false;
                this.refreshTokenSubject.next(tokenData);

                return next.handle(this.addAuthenticationToken(request));
              }),
              catchError((err: any) => {
                this.refreshTokenInProgress = false;

                this.logOut();
                return throwError(err);
              }));
        }
      })
    );
  }

  private saveUserInfo(responceData: ResponseLoginModel) {

    let user = new LocalUser();
    user.authenticationType = responceData.authType;
    user.token = responceData.accessToken;
    user.refreshToken = responceData.refreshToken;

    user.userId = responceData.userId;
    user.username = responceData.username;

    this.localUserService.saveCurrentUserInfo(user);
  }

  private async logOut() {

    const user = this.localUserService.GetCurrentUser();
    if (user == null) {

      this.localStorageService.clearStorage();
      this.router.navigate(['login']);

      return;
    }

    await this.auth.logOut(user.authenticationType.valueOf());

    this.localStorageService.clearStorage();
    this.router.navigate(['login']);
    return;
  }


  private addAuthenticationToken(request) {

    const user = this.localUserService.GetCurrentUser();
    if (!user) {

      return request;
    }

    return request.clone({
      headers: request.headers.set('Authorization', `Bearer ${user.token}`)
    });
  }
}