export enum TaskStatusChangeMode {

    Default = 0,
    ReOpen = 1,
    Start = 2,
    Pause = 3,
    Close = 4,
}
