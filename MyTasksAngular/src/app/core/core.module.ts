import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpHelper } from './helpers';
import { LocalStorageService } from './services/local-storage.service';
import { BaseService, AccountService, TasksService, TasksLocalService, SharedService, AuthGuardService } from './services';
import { LocalUserService } from './services/local-user.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ToastService } from './services/toast.service';
import { JwtModule } from '@auth0/angular-jwt';
import { JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AcessTokenInterceptor } from './interseptors/acess-token.interceptor';
import { RefreshTokenInterceptor } from './interseptors/refresh-token.interceptor';
import { OpposedAuthGuardService } from './services/opposed-auth-guard.service';
import { HttpErrorInterseptor } from './interseptors/http-error.interseptor';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatSnackBarModule,
    JwtModule
  ],
  exports: [],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AcessTokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: RefreshTokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterseptor, multi: true },
    BaseService,
    HttpHelper,
    LocalStorageService,
    LocalUserService,
    AccountService,
    TasksService,
    TasksLocalService,
    SharedService,
    ToastService,
    AuthGuardService,
    { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
    JwtHelperService,
    OpposedAuthGuardService
  ]
})
export class CoreModule { }
