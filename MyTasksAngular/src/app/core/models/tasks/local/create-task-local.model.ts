import { TaskCoordinates } from '../taskcoordinates';

export class CreateTaskLocalModel {
    title: string;
    description: string;
    isStarted: boolean;

    coordinates: TaskCoordinates;
    photo: PhotoModel;
}

export class PhotoModel {
    data:string;
  }
