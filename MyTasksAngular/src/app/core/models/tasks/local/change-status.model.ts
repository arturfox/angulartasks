import { TaskStatus } from '../../../enums/task-status.enum';


export class ChangeStatusModel {

    public taskId: string;
    public taskStatus: TaskStatus;
}
