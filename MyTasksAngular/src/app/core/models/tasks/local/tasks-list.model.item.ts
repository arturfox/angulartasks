import { TaskCoordinates } from '../taskcoordinates';
import { TaskStatus } from '../../../enums/task-status.enum';


export class TasksListModelItem {

    public title: string;
    public description: string;
    public status: TaskStatus;

    public id: string;

    public coordinates: TaskCoordinates;

    constructor() {

    }
}