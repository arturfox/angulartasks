import { TaskStatus } from '../../enums/task-status.enum';
import { Photo } from './photo';
import { TaskCoordinates } from './taskcoordinates';

export class TaskListViewItem {

    public title: string;
    public description: string;
    public status: TaskStatus;
    public id: string;

    public isCreated = (this.status == TaskStatus.Created);
    public isStarted = (this.status == TaskStatus.Started)
    public isFinished = (this.status == TaskStatus.Finished);

    public coordinates: TaskCoordinates;
    public photo: Photo;

    constructor() {

    }
}