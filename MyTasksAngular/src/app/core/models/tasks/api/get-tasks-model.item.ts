import { TaskCoordinates } from '../taskcoordinates';
import { Photo } from '../photo';
import { TaskStatus } from '../../../enums/task-status.enum';

export class GetTasksModelItem {

    public title: string;
    public description: string;
    public status: TaskStatus;

    public id: string;
    public coordinates: TaskCoordinates;
    public photo: Photo;

    constructor() {
    }
}