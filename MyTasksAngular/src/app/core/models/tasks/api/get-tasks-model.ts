import { GetTasksModelItem } from './get-tasks-model.item'

export class GetTasksModel {

    public userId: string;
    public tasks: Array<GetTasksModelItem>;

    constructor() {

    this.tasks=new Array<GetTasksModelItem>();
    
    }
}