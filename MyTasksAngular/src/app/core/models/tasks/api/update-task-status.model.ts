import { TaskStatus } from '../../../enums';

export class UpdateTaskStatusModel {

    public taskId : string;
    public status: TaskStatus;
}
