import { TaskCoordinates } from '../taskcoordinates';
import { PhotoModel } from '../local/create-task-local.model';

export class CreateTaskModel{

    public title:string;
    public description:string;
    public isStarted:  boolean;

    public coordinates: TaskCoordinates;
    public photo: PhotoModel;
    
    constructor()
    {

    }  
}