import { AuthenticationType } from '../../../enums/authentication-type.enum';


export class RequestLoginBaseModel {

    public username: string;
    public authorizationType: AuthenticationType;    
    public accessToken: string;
}
