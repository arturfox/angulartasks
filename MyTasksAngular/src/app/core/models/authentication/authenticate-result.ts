import { AuthenticationType } from '../../enums/authentication-type.enum';


export class AuthenticateResult {

    public username: string;
    public accessToken: string;
    public refreshToken: string;
    public userId: string;
    public authType: AuthenticationType;
    
    public isSuceed: boolean;
    public message: string;
}
