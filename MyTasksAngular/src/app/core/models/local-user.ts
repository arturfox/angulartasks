import { AuthenticationType } from '../enums/authentication-type.enum';


export class LocalUser {
    
    public username: string;
    public userId: string;
    public authenticationType : AuthenticationType;
    public token: string;
    public refreshToken: string;
}
