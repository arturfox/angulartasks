import { Component, ViewChild, OnInit, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService, LogOutResult, SharedService, MenuAction } from '../../core';
import { MatMenuTrigger } from '@angular/material';
import { MatMenu } from '@angular/material';
import { fromEvent, Observable, Subscription } from 'rxjs';
import { map, debounceTime } from 'rxjs/operators';


@Component({
  selector: 'navbar-panel',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild(MatMenuTrigger, { static: false }) trigger: MatMenuTrigger;
  @ViewChild(MatMenu, { static: false }) menu: MatMenu;
  @ViewChild('Search', { static: false }) searchField: ElementRef;

  private SEARCH_DELAY_MILLISECONDS: number = 750;
  private searchSubscription: Subscription;
  private searchQuery$: Observable<any>;

  constructor(private localStorageService: LocalStorageService,
    private router: Router,
    private sharedService: SharedService) { }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {

    this.searchQuery$ = fromEvent(this.searchField.nativeElement, "input")
      .pipe(map(x => (x as any).target.value),
        debounceTime(this.SEARCH_DELAY_MILLISECONDS));

  this.searchSubscription =  this.searchQuery$.subscribe((data) => {
      this.sharedService.onMenuSearchPerfomed.emit(data);
    });

  }

  ngOnDestroy(): void {

    this.searchSubscription.unsubscribe();
  }

  createTaskSelected() {

    this.sharedService.onMenuItemSelected.emit(MenuAction.CreateTask);

  }

  tasksSelected() {

    this.router.navigate(['home/tasks']);

  }

  onLogOutResultObtained(logOutData: LogOutResult) {

    if (logOutData.isSuceed) {

      this.localStorageService.clearStorage();
      this.router.navigate(['login']);
      return;
    }
  }
}
