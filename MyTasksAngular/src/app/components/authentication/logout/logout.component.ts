import { Component, OnInit, EventEmitter, Output  } from '@angular/core';
import { Router } from '@angular/router';
import { LogOutResult, AccountService, LocalUserService } from '../../../core';
import { ToastService } from '../../../core/services/toast.service';

@Component({
  selector: 'auth-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {


  @Output() logOutResultObtained = new EventEmitter<LogOutResult>();

  constructor(private accountService: AccountService,
              private localUserUservice: LocalUserService,
              private router: Router,
              private toastService: ToastService) { }

  ngOnInit() {

  }

  async logOut(){

    let logOutModel = new LogOutResult();
    
    let user = this.localUserUservice.GetCurrentUser();
    if(user == null){
      

      logOutModel.isSuceed = true;

      this.logOutResultObtained.emit(logOutModel);

      return;
    }
    
    let result = await this.accountService.logOut(user.authenticationType.valueOf());

    logOutModel.isSuceed = result.isSuceed;
    logOutModel.message = result.message;

    if(logOutModel.isSuceed){

      this.logOutResultObtained.emit(logOutModel);
      return;      
    }

    if(result.statusCode){
      this.toastService.showMessage(logOutModel.message);
    }
  }

}
