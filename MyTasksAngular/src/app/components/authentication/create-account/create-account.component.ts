import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { RegistrationResult, AccountService, RegisterModel } from '../../../core';


@Component({
  selector: 'auth-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss']
})
export class CreateAccountComponent implements OnInit {

  registrationFormGroup: FormGroup;

  @Output() registrationResultObtained = new EventEmitter<RegistrationResult>();

  constructor(private _formBuilder: FormBuilder,
    private accountService: AccountService) {

  }

  ngOnInit() {

    this.registrationFormGroup = this._formBuilder.group({
      emailCtrl: ['', Validators.required],
      passwordCtrl: ['', Validators.required],
      confirmPasswordCtrl: ['', Validators.required]
    });
  }


  onSubmit() {

    let registerModel = new RegisterModel();

    registerModel.username = (this.registrationFormGroup.get('emailCtrl') as FormControl).value;
    registerModel.password = (this.registrationFormGroup.get('passwordCtrl') as FormControl).value;
    registerModel.confirmPassword = (this.registrationFormGroup.get('confirmPasswordCtrl') as FormControl).value;

    this.accountService.signIn(registerModel).subscribe((result) => {

      let registerResult = new RegistrationResult();
      registerResult.isSuceed = result.isSuceed;
      if (result.statusCode) {
        registerResult.message = result.message;
      }

      this.registrationResultObtained.emit(registerResult);
    });
  }

}