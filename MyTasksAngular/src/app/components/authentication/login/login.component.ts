import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthenticateResult, AccountService, RequestLoginModel, AuthenticationType, ResponseLoginModel, ResultModel } from '../../../core';


@Component({
  selector: 'auth-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  loginForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl('')
  });

  @Output() onAuthenticateResultObtained = new EventEmitter<AuthenticateResult>();

  constructor(private accountService: AccountService) {

  }

  loginGoogle() {

    this.accountService.loginGooglePlus().subscribe((loginResult) => this.handleLoginResult(loginResult));
  }

  loginFacebook() {

    this.accountService.loginFacebook().subscribe((loginResult) => this.handleLoginResult(loginResult));
  }

  onSubmit() {

    let loginModel = new RequestLoginModel();
    loginModel.password = (this.loginForm.get('password') as FormControl).value;
    loginModel.username = (this.loginForm.get('email') as FormControl).value;
    loginModel.authorizationType = AuthenticationType.Default;

    this.accountService.login(loginModel).subscribe((result) => this.handleLoginResult(result));
  }

  private handleLoginResult(data: ResultModel<ResponseLoginModel>): void {

    let authenticateResult = new AuthenticateResult();
    authenticateResult.isSuceed = data.isSuceed;
    if(data.statusCode){
      
      authenticateResult.message = data.message; 
    }

    if (data.isSuceed) {
      authenticateResult.accessToken = data.data.accessToken;
      authenticateResult.refreshToken = data.data.refreshToken;
      authenticateResult.authType = data.data.authType;
      authenticateResult.userId = data.data.userId;
      authenticateResult.username = data.data.username;
    }

    this.onAuthenticateResultObtained.emit(authenticateResult);
  }
}