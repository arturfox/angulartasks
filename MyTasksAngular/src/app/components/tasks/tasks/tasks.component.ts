import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { TasksListComponent } from '../tasks-list/tasks-list.component';
import { TaskInfoComponent } from '../task-info/task-info.component';
import { MatDrawer } from '@angular/material/sidenav';
import { MatDialog } from '@angular/material/dialog';
import {
  TasksListModelItem,
  ChangeStatusModel,
  TaskListViewItem,
  TaskStatus,
  TasksService,
  TasksLocalService,
  TasksListModel,
  GetTasksModelItem,
  EditTaskLocalModel,
  CreateTaskLocalModel,
  CreateTaskModel,
  DeleteTaskModel,
  UpdateTaskStatusModel,
  EditTaskModel,
  TaskStatusChangeMode,
  SharedService,
  MenuAction,
  NotificationReceiverService
} from '../../../core';
import { CreateTaskComponent } from '../create-task/create-task.component';
import { DeleteDialogComponent } from '../delete-dialog/delete-dialog.component';
import { ChangeStatusDialogComponent } from '../change-status-dialog/change-status-dialog.component';
import { ToastService } from '../../../core/services/toast.service';
import { Subscription, interval } from 'rxjs';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit, OnDestroy {

  public tasksSourceUI: Array<TaskListViewItem>;
  public statusType: any = TaskStatus;

  @ViewChild("TasksListComponent", { static: false }) tasksListElement: TasksListComponent;
  @ViewChild("TaskInfoComponent", { static: false }) taskInfoElement: TaskInfoComponent;
  @ViewChild("drawer", { static: false }) leftPanel: MatDrawer;

  private REFRESH_TASKS_LIST_INTERVAL_MILLISECONDS: number = 18600000;
  private tasks: Array<TasksListModelItem>;
  private selectedTaskId: string;
  private changeTaskStatus: ChangeStatusModel;
  private deleteTaskId: string;
  private tasksSource: Array<TaskListViewItem>;
  private subscriptions: Subscription[];

  constructor(private tasksService: TasksService,
    private localTasksService: TasksLocalService,
    private sharedService: SharedService,
    public dialog: MatDialog,
    private toastService: ToastService,
    private notificationReceiver: NotificationReceiverService) {

    this.tasks = new Array<TasksListModelItem>();
    this.tasksSource = new Array<TaskListViewItem>();
    this.tasksSourceUI = new Array<TaskListViewItem>();
    this.subscriptions = [];
  }


  async ngOnInit() {

    this.setData();

    this.subscriptions.push(this.sharedService.onMenuItemSelected.subscribe((data: MenuAction) => {

      if (data == MenuAction.CreateTask) {

        this.openCreateTaskDialog();
      }
    }));

    this.subscriptions.push(this.sharedService.onMenuSearchPerfomed.subscribe((query: string) => {

      this.tasksSourceUI = this.filterTasksByTitle(query, this.tasksSource);
    }));


    /*this.subscriptions.push(interval(this.REFRESH_TASKS_LIST_INTERVAL_MILLISECONDS).subscribe(async () => {

      this.setData();
    }));*/

    this.subscriptions.push(this.notificationReceiver.receiveNotifications().subscribe((data)=>{

      this.setData();
  
    }));
  }

  ngOnDestroy(): void {

    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  setData(): void {

    this.tasksService.getUserTasks().subscribe(data => {

      if (!data.isSuceed) {

        return;
      }

      this.tasks.length = 0;
      data.data.tasks.forEach((obj, index) => {
        let elem = new TasksListModelItem();
        elem.id = obj.id;
        elem.description = obj.description;
        elem.title = obj.title;
        elem.status = obj.status;
        elem.coordinates = obj.coordinates;

        this.tasks.push(elem);
      });

      this.setTasksViewList(data.data.tasks);

      var model = new TasksListModel();
      model.tasks = this.tasks;

      this.localTasksService.UpdateUsersTaks(model);

    });

  }

  private setTasksViewList(getTasks: GetTasksModelItem[]): void {

    this.tasksSource.length = 0;
    getTasks.forEach((obj, index) => {

      let viewElem = new TaskListViewItem();
      viewElem.id = obj.id;
      viewElem.description = obj.description;
      viewElem.title = obj.title;
      viewElem.status = obj.status;
      viewElem.coordinates = obj.coordinates;
      viewElem.photo = obj.photo;

      this.tasksSource.push(viewElem);
    });

    this.tasksSourceUI = this.tasksSource;
  }

  onSelectedTaskChanged(taskId: string) {

    if (this.selectedTaskId == taskId) {

      this.leftPanel.toggle();
      return;
    }

    this.selectedTaskId = taskId;
    let task = this.tasksSource.find(task => task.id === taskId);
    this.taskInfoElement.setTaskInfo(task);

    if (this.leftPanel.opened) {
      return;
    }
    this.leftPanel.toggle();
  }

  onChangeStatusFired(statusModel: ChangeStatusModel) {

    this.openChangeStatusDialog(statusModel);
  }

  onDeleteStatusFired(taskId: string) {

    this.openDeleteTaskDialog(taskId);
  }

  async onSaveChangesFired(data: EditTaskLocalModel) {

    await this.saveChanges(data);
  }

  openCreateTaskDialog(): void {

    const dialogRef = this.dialog.open(CreateTaskComponent, {
      minWidth: '850px',
      minHeight: '550px',
    });

    dialogRef.afterClosed().subscribe(result => {

      if (result) {

        this.createTask(result);
      }
    });

  }


  openDeleteTaskDialog(taskId: string): void {

    this.deleteTaskId = null;

    let task = this.tasksSource.find(task => task.id === taskId);
    if (task == null) {

      return;
    }

    this.deleteTaskId = taskId;

    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '250px',
      data: ["Delete Task", "Are you sure you want to remove task?"]
    });

    dialogRef.afterClosed().subscribe(result => {

      if (result) {

        this.deleteTask(this.deleteTaskId);
      }

      this.deleteTaskId = null;
    });
  }

  openChangeStatusDialog(statusModel: ChangeStatusModel): void {

    this.changeTaskStatus = null;

    let task = this.tasksSource.find(task => task.id === statusModel.taskId);
    if (task == null) {

      return;
    }

    this.changeTaskStatus = statusModel;

    let statusMode = this.getTaskStatusChangeModeByTaskStatuses(task.status, statusModel.taskStatus);

    const dialogRef = this.dialog.open(ChangeStatusDialogComponent, {
      width: '250px',
      data: statusMode
    });

    dialogRef.afterClosed().subscribe(result => {

      if (result) {

        this.updateTaskStatus(this.changeTaskStatus);
      }

      this.changeTaskStatus = null;

    });
  }

  async createTask(model: CreateTaskLocalModel): Promise<void> {

    if (model == null) {
      return;
    }

    let createdTask = new CreateTaskModel();
    createdTask.description = model.description;
    createdTask.title = model.title;
    createdTask.isStarted = model.isStarted;
    createdTask.coordinates = model.coordinates;
    createdTask.photo = model.photo;

    this.tasksService.createTask(createdTask).subscribe(async (data) => {

      if (data.isSuceed) {

        await this.setData();        
        return;
      }

      if(data.statusCode){

        this.toastService.showMessage(data.message);
      }
    });

  }


  async deleteTask(id: string): Promise<void> {

    let deleteModel = new DeleteTaskModel();
    deleteModel.id = id;

    this.tasksService.deleteTaskById(deleteModel).subscribe(async (data) => {

      if (data.isSuceed) {

        this.leftPanel.close();
        await this.setData();
        return;
      }

      if(data.statusCode){
        this.toastService.showMessage(data.message);
      }
    });

  }


  async updateTaskStatus(changeStatus: ChangeStatusModel): Promise<void> {

    let updateStatusModel = new UpdateTaskStatusModel();
    updateStatusModel.status = changeStatus.taskStatus;
    updateStatusModel.taskId = changeStatus.taskId;

    this.tasksService.updateTaskStatus(updateStatusModel).subscribe(async (data) => {

      if (!data.isSuceed) {

        this.toastService.showMessage(data.message);
        return;
      }

      this.leftPanel.close();
      await this.setData();
    });

  }

  async saveChanges(data: EditTaskLocalModel): Promise<void> {

    let editModel = new EditTaskModel();
    editModel.id = data.id;
    editModel.description = data.description;

    this.tasksService.editTask(editModel).subscribe(async (data) => {

      if (!data.isSuceed) {

        this.toastService.showMessage(data.message);
        return;
      }

      this.leftPanel.close();
      await this.setData();
    });

  }

  filterTasksByTitle(query: string, source: Array<TaskListViewItem>): Array<TaskListViewItem> {

    return source.filter(x => x.title.toLowerCase().includes(query.toLowerCase()));
  }

  getTaskStatusChangeModeByTaskStatuses(currentStatus: TaskStatus, newStatus: TaskStatus): TaskStatusChangeMode {

    if (newStatus == TaskStatus.Finished) {

      return TaskStatusChangeMode.Close;
    }

    if (newStatus == TaskStatus.Started && currentStatus == TaskStatus.Created) {

      return TaskStatusChangeMode.Start;
    }

    if (newStatus == TaskStatus.Started && currentStatus == TaskStatus.Finished) {

      return TaskStatusChangeMode.ReOpen;
    }

    if (newStatus == TaskStatus.Created) {

      return TaskStatusChangeMode.Pause;
    }

    return TaskStatusChangeMode.Default;
  }

}