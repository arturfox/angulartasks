import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TasksListComponent } from './tasks-list/tasks-list.component';
import { TasksRoutingModule } from './tasks-routing.module';
import { TaskInfoComponent } from './task-info/task-info.component';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDialogModule } from '@angular/material/dialog';
import { ChangeStatusDialogComponent } from './change-status-dialog/change-status-dialog.component';
import { DeleteDialogComponent } from './delete-dialog/delete-dialog.component';
import { CreateTaskComponent } from './create-task/create-task.component';
import { MatStepperModule } from '@angular/material/stepper';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule, MatCheckboxModule, MatCardModule, MatSortModule } from '@angular/material';
import { MapModule } from '../map/map.module';
import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
import { TasksComponent } from './tasks/tasks.component';
import { SharedModule } from '../../shared/shared.module';

// TODO: move <create task logic> from tasks component to create-task-dialog component
@NgModule({
  declarations: [TasksListComponent, TaskInfoComponent, TasksComponent, ChangeStatusDialogComponent, DeleteDialogComponent, CreateTaskComponent],
  imports: [
    CommonModule,
    TasksRoutingModule,
    MapModule,
    SharedModule,

    ReactiveFormsModule,
    MatNativeDateModule,
    MatSidenavModule,
    MatDialogModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatCardModule,
    MatSortModule,
    FileUploadModule
  ],
  entryComponents: [ChangeStatusDialogComponent, DeleteDialogComponent, CreateTaskComponent]
})
export class TasksModule { }
