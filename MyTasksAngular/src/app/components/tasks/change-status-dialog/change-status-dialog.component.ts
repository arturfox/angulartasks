import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TaskStatusChangeMode } from '../../../core';

@Component({
  selector: 'tasks-change-status-dialog',
  templateUrl: './change-status-dialog.component.html',
  styleUrls: ['./change-status-dialog.component.scss']
})
export class ChangeStatusDialogComponent {

  public title: string;
  public subTitle: string;

  constructor(
    public dialogRef: MatDialogRef<ChangeStatusDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TaskStatusChangeMode) {

    this.setHeaders(data);
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  setHeaders(status: TaskStatusChangeMode) {

    if (status == TaskStatusChangeMode.Close) {

      this.title = "Close Task";
      this.subTitle = "Are you sure you want to close task?";
      return;
    }
    if (status == TaskStatusChangeMode.Pause) {

      this.title = "Pause Task";
      this.subTitle = "Are you sure you want to stop task?";
      return;
    }
    if (status == TaskStatusChangeMode.ReOpen) {

      this.title = "Re-open Task";
      this.subTitle = "Are you sure you want to restart task?";
      return;
    }
    if (status == TaskStatusChangeMode.Start) {

      this.title = "Start Task";
      this.subTitle = "Are you sure you want to start task?";
      return;
    }
  }

}