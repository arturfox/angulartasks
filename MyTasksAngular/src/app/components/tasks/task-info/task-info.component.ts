import { Component, OnInit, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { MapComponent } from '../../map/map.component';
import { FormControl } from '@angular/forms';
import { ChangeStatusModel, EditTaskLocalModel, TaskListViewItem, TaskStatus } from '../../../core';

@Component({
  selector: 'tasks-task-info',
  templateUrl: './task-info.component.html',
  styleUrls: ['./task-info.component.scss']
})
export class TaskInfoComponent implements OnInit, AfterViewInit {

  @Output() changeStatusFired = new EventEmitter<ChangeStatusModel>();
  @Output() deleteTaskFired = new EventEmitter<string>();
  @Output() saveChangesFired = new EventEmitter<EditTaskLocalModel>();

  @ViewChild("customMapComponent", { static: false }) mapElement: MapComponent;

  descriptionCtrl: FormControl;

  selectedTask: TaskListViewItem;
  statusType: any = TaskStatus;

  coordinates: any;
  isChangesDetected: boolean = false;

  constructor() {

    this.descriptionCtrl = new FormControl('');

   }

  ngOnInit() {

  }

  async ngAfterViewInit() {

  }

  async setTaskInfo(task: TaskListViewItem) {

    this.selectedTask = task;
    this.coordinates = task.coordinates;

    this.descriptionCtrl.setValue(task.description);
    this.isChangesDetected = false;

    if (this.selectedTask.coordinates == null) {
      return;
    }

    const mapComp = this.mapElement;
    await mapComp.loadMap();
    mapComp.updateMarker(this.selectedTask.coordinates.lat, this.selectedTask.coordinates.lng, this.selectedTask.title, null);
  }

  saveChanges() {

    let editModel = new EditTaskLocalModel();
    editModel.description = this.descriptionCtrl.value;
    editModel.id = this.selectedTask.id;

    this.saveChangesFired.emit(editModel);
  }

  finishTask() {

    this.changeStatus(TaskStatus.Finished);
  }

  replayTask() {

    this.changeStatus(TaskStatus.Started);
  }

  stopTask() {

    this.changeStatus(TaskStatus.Created);
  }

  startTask() {

    this.changeStatus(TaskStatus.Started);
  }


  changeStatus(status: TaskStatus) {

    if (this.selectedTask == undefined) {
      return;
    }

    let model = this.getChangeStatusModel(status, this.selectedTask.id);
    this.changeStatusFired.emit(model);
  }

  deleteTask() {

    if (this.selectedTask == undefined) {
      return;
    }

    this.deleteTaskFired.emit(this.selectedTask.id);
  }

  private getChangeStatusModel(status: TaskStatus, id: string) {

    let model = new ChangeStatusModel();
    model.taskId = id;
    model.taskStatus = status;

    return model;
  }

}
