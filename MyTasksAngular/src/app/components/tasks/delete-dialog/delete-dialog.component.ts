import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'tasks-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss']
})
export class DeleteDialogComponent implements IBaseDialogComponent {

 public title: string;
 public subTitle: string;

  constructor(public dialogRef: MatDialogRef<DeleteDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: string[]) { 

                this.setHeaders(data);
              }
  
  onCancelClick(): void {
    this.dialogRef.close();
  }

  setHeaders(titles: string[]) {

    this.title = titles[0];
    this.subTitle = titles[1];
  }

}

export interface IBaseDialogComponent {

  title: string;
  subTitle: string;
  setHeaders(data: any);
  onCancelClick(): void;
}
