import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TaskListViewItem, TaskStatus } from '../../../core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

@Component({
  selector: 'tasks-taskslist',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.scss'],
  animations: [
    trigger('FadeInAnimation', [
      state('in', style({ opacity: 1 })),
      transition(':enter', [
        style({ opacity: 0 }),
        animate(1200)
      ]),
      transition(':leave',
        animate(1200, style({ opacity: 0 })))
    ])
  ]
})
export class TasksListComponent implements OnInit {

  @Input() tasksList: Array<TaskListViewItem>;
  @Output() showMoreInfoSelected = new EventEmitter<string>();

  public statusType: any = TaskStatus;

  constructor() {

    this.tasksList = new Array<TaskListViewItem>();
  }

  async ngOnInit() {

  }

  showMore(event:any) {

    this.showMoreInfoSelected.emit(event.target.id);
  }

}
