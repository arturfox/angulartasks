import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { FileUploader } from 'ng2-file-upload';
import { CreateTaskLocalModel, TaskCoordinates, PhotoModel } from '../../../core';
import { ToastService } from '../../../core/services/toast.service';

@Component({
  selector: 'tasks-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.scss']
})
export class CreateTaskComponent implements OnInit {

  baseInfoFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  mapFormGroup: FormGroup;


  public lat: any;
  public lng: any;

  public position: string;

  hasBaseDropZoneOver: boolean;
  hasAnotherDropZoneOver: boolean;
  uploader: FileUploader;

  constructor(private _formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<CreateTaskComponent>,
    private toastService: ToastService) {

    this.uploader = new FileUploader({
      url: '',
      disableMultipart: true, // 'DisableMultipart' must be 'true' for formatDataFunction to be called.
      allowedMimeType: ['image/jpeg'],
      formatDataFunctionIsAsync: true,
      formatDataFunction: async (item) => {
        return new Promise((resolve, reject) => {
          resolve({
            name: item._file.name,
            length: item._file.size,
            contentType: item._file.type,
            date: new Date()
          });
        });
      }
    });

    this.hasBaseDropZoneOver = false;
    this.hasAnotherDropZoneOver = false;

    this.uploader.onWhenAddingFileFailed = (item) => {

      this.toastService.showMessage("only image/jpeg file format");
    }

    this.uploader.onAfterAddingFile = (item) => {

      if (this.uploader.queue.length == 0) {

        return;
      }

      let lastItem = this.uploader.queue[this.uploader.queue.length - 1];
      this.uploader.queue = [lastItem];
    }

  }

  ngOnInit() {
    this.baseInfoFormGroup = this._formBuilder.group({
      titleCtrl: ['', Validators.required],
      descriptionCtrl: ['', Validators.required],
      isTaskStartedCtrl: ['']
    });
    this.secondFormGroup = this._formBuilder.group({

    });
    this.mapFormGroup = this._formBuilder.group({

      isLocationPinnedCtrl: ['']
    });
  }

  setLongitude(lng: any): void {

    this.lng = lng;

    if (this.lng == null || this.lat == null) {
      return;
    }

    this.position = `${this.lat.toString()} ${this.lng.toString()}`;
  }

  setLatinude(lat: any): void {

    this.lat = lat;

    if (this.lat == null || this.lng == null) {
      return;
    }

    this.position = `${this.lat.toString()} ${this.lng.toString()}`;
  }

  async onConfirm(): Promise<void> {

    let createTaskModel = new CreateTaskLocalModel();
    createTaskModel.title = (this.baseInfoFormGroup.get('titleCtrl') as FormControl).value;
    createTaskModel.description = (this.baseInfoFormGroup.get('descriptionCtrl') as FormControl).value;
    createTaskModel.isStarted = (this.baseInfoFormGroup.get('isTaskStartedCtrl') as FormControl).value;

    let pinLocation = (this.mapFormGroup.get('isLocationPinnedCtrl') as FormGroup).value;

    if (pinLocation && this.lat != undefined && this.lng != undefined) {
      createTaskModel.coordinates = new TaskCoordinates();
      createTaskModel.coordinates.lat = this.lat;
      createTaskModel.coordinates.lng = this.lng;
    }

    if (this.uploader.queue != null && this.uploader.queue.length != 0) {

      let file = this.uploader.queue[0]._file;
      let reader: FileReader = new FileReader();

      reader.onloadend = (e) => {
        let data = reader.result;
        createTaskModel.photo = new PhotoModel();
        createTaskModel.photo.data = data as string;

        this.dialogRef.close(createTaskModel);
      }
      reader.readAsDataURL(file);

      return;
    }

    this.dialogRef.close(createTaskModel);
  }


  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }

}