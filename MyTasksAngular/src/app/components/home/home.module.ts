import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { TasksModule } from '../tasks/tasks.module';
import { NavbarModule } from '../navbar/navbar.module';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    HomeRoutingModule,
    CommonModule,
    TasksModule,
    NavbarModule,
  ]
})
export class HomeModule { }
 