import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home.component';


const routes: Routes = [
    {
        path: '', component: HomeComponent, children: [
            { 
                path: 'tasks', loadChildren: () => import('../tasks/tasks.module').then(mod => mod.TasksModule) 
            },
            {
                path: '', redirectTo: 'tasks', pathMatch: 'full'
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class HomeRoutingModule { }