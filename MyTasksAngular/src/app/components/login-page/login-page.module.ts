import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginPageComponent } from './login-page.component';
import { AuthenticationModule } from '../authentication/authentication.module';
import { LoginPageRoutingModule } from './login-page-routing.module';

@NgModule({
  declarations: [LoginPageComponent],
  imports: [
    CommonModule,
    AuthenticationModule,
    LoginPageRoutingModule   
  ],
  exports:[LoginPageComponent],
})
export class LoginPageModule { }
