import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalUserService, AuthenticateResult, LocalUser, LocalStorageService } from '../../core';
import { ToastService } from '../../core/services/toast.service';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';


@Component({
  selector: 'app-login-page',
  templateUrl: 'login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
  animations: [
    trigger('simpleFadeAnimation', [
      state('in', style({ opacity: 1 })),
      transition(':enter', [
        style({ opacity: 0 }),
        animate(1000)
      ]),
      transition(':leave',
        animate(600, style({ opacity: 0 })))
    ]),
    trigger('blinkValidateAnimation', [
      state('valid', style({ 'background-color': 'transparent' })),
      state('invalid', style({ 'background-color': '#cc0000' })),
      transition('valid => invalid', [
        animate('0.3s')
      ]),
      transition('invalid => valid', [
        animate('0.4s')
      ])
    ]),
    trigger('slideInOut', [
      transition(':enter', [
        style({ transform: 'translateY(-100%)' }),
        animate('700ms ease-in', style({ transform: 'translateY(0%)' }))
      ]),
      transition(':leave', [
        animate('700ms ease-in', style({ transform: 'translateY(-100%)' }))
      ])
    ])
  ]
})
export class LoginPageComponent implements OnInit {

  isValid: boolean = true;


  constructor(private router: Router,
    private localUserService: LocalUserService,
    private toastService: ToastService,
    private localStorageService: LocalStorageService, ) {

  }

  ngOnInit() {

    this.localStorageService.clearStorage();
  }

  async setLoggedUser(loginData: AuthenticateResult) {

    if (!loginData.isSuceed && loginData.message) {

      this.toastService.showMessage(loginData.message);
    }

    if (!loginData.isSuceed) {

      this.isValid = false;
      return;
    }

    let user = new LocalUser();
    user.authenticationType = loginData.authType;
    user.token = loginData.accessToken;
    user.refreshToken = loginData.refreshToken;

    user.userId = loginData.userId;
    user.username = loginData.username;

    this.localUserService.saveCurrentUserInfo(user);

    await this.router.navigate(['home']);
  }

  async navigateSignUp() {

    await this.router.navigate(['registration']);
  }

  onAnimationEvent() {

    if (this.isValid) {
      return;
    }

    this.isValid = !this.isValid;
  }
}
