import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPageComponent } from './login-page.component';
import { OpposedAuthGuardService } from '../../core/services/opposed-auth-guard.service';

const routes: Routes = [
    { path: '', component: LoginPageComponent, canActivate: [OpposedAuthGuardService] },
    { path: 'login', redirectTo: "", pathMatch: "full" }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class LoginPageRoutingModule { }