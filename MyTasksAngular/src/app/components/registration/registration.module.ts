import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewAccountComponent } from './new-account.component';
import { AuthenticationModule } from '../authentication/authentication.module';
import { RegistrationRoutingModule } from './registration-routing.module';

@NgModule({
  declarations: [NewAccountComponent],
  imports: [
    CommonModule,
    AuthenticationModule,
    RegistrationRoutingModule
  ],
  exports: [NewAccountComponent]
})
export class RegistrationModule { }
