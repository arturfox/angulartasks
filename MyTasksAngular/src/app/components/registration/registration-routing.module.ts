import { NgModule } from '@angular/core';
import { Routes , RouterModule } from '@angular/router';
import { NewAccountComponent } from './new-account.component';
import { OpposedAuthGuardService } from '../../core/services/opposed-auth-guard.service';

const routes: Routes = [
    { path: '', redirectTo:"newAccount"},
    { path: 'newAccount', component: NewAccountComponent, canActivate:[OpposedAuthGuardService] }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class RegistrationRoutingModule {

}