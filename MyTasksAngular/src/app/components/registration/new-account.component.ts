import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegistrationResult, ToastService } from '../../core';

@Component({
  selector: 'app-new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.scss']
})
export class NewAccountComponent implements OnInit {

  constructor(private router: Router,
    private toastService: ToastService) { }

  ngOnInit() {
  }

  onRegistrationResultObtained(result: RegistrationResult) {

    if (result.isSuceed) {

      this.router.navigate(['login']);
      return;
    }

    if (result.message) {

      this.toastService.showMessage(result.message);
    }
  }
}
