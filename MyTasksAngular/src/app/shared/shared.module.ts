import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StringTrimPipe } from './pipes/string-trim.pipe';
import { HighlightDirective } from './directives/highlight.directive';

@NgModule({
  declarations: [StringTrimPipe, HighlightDirective],
  exports: [StringTrimPipe, HighlightDirective],
  imports: [CommonModule]
})
export class SharedModule { }
