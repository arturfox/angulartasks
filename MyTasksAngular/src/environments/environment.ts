export const environment = {
  production: false
};

export const SERVER_URL = 'http://999.99.99.99:5575/';
export const GOOGLE_MAPS_API_KEY = 'maps_key';
export const WEB_CLIENT_ID_GOOGLE = 'WEB_CLIENT_ID_GOOGLE';
export const APP_ID_FACEBOOK = 'APP_ID_FACEBOOK';